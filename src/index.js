import React from "react";
import ReactDOM from "react-dom";
import App from "./home/project";
import "./styles/button.less";
import "./styles/layout.less";
import "./styles/form.less";
import "./styles/input.less";
import "./styles/login.less";
import "./styles/footer.less";
import "./styles/text.less";
import "./styles/sider.less";
import "./styles/icon.less";
import "./styles/header.less";
import "./styles/select.less";
import "./styles/checkbox.less";
import "./styles/table.less";

ReactDOM.render(<App />, document.getElementById("root"));
