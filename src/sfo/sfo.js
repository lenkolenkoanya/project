import React, { useState, useEffect } from "react";
import axios from "axios";
import "antd/dist/antd.css";
import { Table, Button, Icon } from "antd";
import { Link } from "react-router-dom";

const columns = [
  {
    title: "Id",
    dataIndex: "id",
    key: "id"
  },
  {
    title: "Наименование",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Кратко",
    dataIndex: "short",
    key: "short"
  },
  {
    title: "Резидент",
    dataIndex: "isResident",
    key: "isResident",
    render: (value, row, index) => {
      if (value === true) {
        return <span>Да</span>;
      }
      return <span>Нет</span>;
    }
  },
  {
    title: "Уставной фонд",
    dataIndex: "fund",
    key: "fund"
  },
  {
    title: "УНП",
    dataIndex: "unp",
    key: "unp"
  },
  {
    title: "Банк",
    dataIndex: "bank",
    key: "bank"
  },
  {
    title: "Дата гос.регистрации",
    dataIndex: "date",
    key: "date"
  },
  {
    title: "Кем выдан",
    dataIndex: "issuedBy",
    key: "issuedBy"
  },
  {
    title: "Налоговая инспекция",
    dataIndex: "tax",
    key: "tax"
  }
];

function Sfo() {
  const [sfo, setSfo] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:3000/sfo").then(resp => {
      const sfo = resp.data;
      setSfo(sfo);
    });
  }, []);

  let handleClick = () => {
    setSelectedRowKeys(selectedRowKeys);
  };

  //select checkbox
  let onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };

  const data = sfo.map(org => {
    return {
      id: org.id,
      name: org.name,
      short: org.short,
      isResident: org.isResident,
      fund: org.fund,
      unp: org.unp,
      bank: org.bank,
      date: org.date,
      issuedBy: org.issuedBy,
      tax: org.tax
    };
  });

  let handleDelete = e => {
    e.preventDefault();

    axios.delete(`http://localhost:3000/sfo/${selectedRowKeys[0]}`);

    axios.get("http://localhost:3000/sfo").then(resp => {
      const newSfo = resp.data;
      setSfo(newSfo);
    });
  };

  const hasSelected = selectedRowKeys.length > 0;
  return (
    <>
      <span className="addSfoTable">Специальные финансовые организации</span>
      <Link to="/cabinet/sfo">
        <Button type="primary" className="refreshButton">
          <span className="addBtnText">Обновить</span>
        </Button>
      </Link>
      <div className="sfoTable">
        <Link to="/cabinet/sfo/add">
          <Button type="link" className="buttonAdd">
            <Icon type="plus-circle" />
            <span className="textBtn">Создать</span>
          </Button>
        </Link>
        <Link to={`/cabinet/sfo/edit/${selectedRowKeys[0]}`}>
          <Button
            type="link"
            onClick={handleClick}
            disabled={!hasSelected}
            className="buttonAdd"
          >
            <Icon type="edit" />
            <span className="textBtn">Изменить</span>
          </Button>
        </Link>

        <Button
          type="link"
          onClick={handleDelete}
          disabled={!hasSelected}
          className="buttonAdd"
        >
          <Icon type="delete" />
          <span className="textBtn">Удалить</span>
        </Button>

        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          rowKey="id"
          className="table"
        />
      </div>
    </>
  );
}
export default Sfo;
