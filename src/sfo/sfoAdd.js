import React, { useState } from "react";
import "antd/dist/antd.css";
import axios from "axios";
import { Form, Input, Button, Checkbox, Select, DatePicker, Icon } from "antd";
import { useHistory, Link } from "react-router-dom";
import moment from "moment";

const { Option } = Select;

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function SfoAdd(props) {
  let history = useHistory();
  const { getFieldDecorator } = props.form;

  const [name, setName] = useState("");
  const [short, setShort] = useState("");
  const [isResident, setIsResident] = useState(false);
  const [fund, setFund] = useState(0);
  const [unp, setUnp] = useState(0);
  const [bank, setBank] = useState("");
  const [date, setDate] = useState("");
  const [tax, setTax] = useState("");
  const [issuedBy, setIssuedBy] = useState("");

  let nameHandleChange = e => {
    setName(e.target.value);
  };

  let shortHandleChange = e => {
    setShort(e.target.value);
  };

  let fundHandleChange = e => {
    setFund(e.target.value);
  };

  let unpHandleChange = e => {
    setUnp(e.target.value);
  };

  let dateHandleChange = date => {
    date = date.format("DD.MM.YYYY");
    setDate(date);
  };

  let issuedByHandleChange = e => {
    setIssuedBy(e.target.value);
  };

  let isResidentHandleChange = e => {
    setIsResident(`${e.target.checked}`);
  };

  let taxHandleChange = value => {
    setTax(value);
  };

  let bankHandleChange = value => {
    setBank(value);
  };

  let handleSubmit = e => {
    console.log("handleSubmit");
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        const sfo = {
          name: name,
          short: short,
          isResident: isResident,
          fund: fund,
          unp: unp,
          bank: bank,
          date: date,
          issuedBy: issuedBy,
          tax: tax
        };

        axios.post("http://localhost:3000/sfo", sfo).then(() => {
          history.push("/cabinet/sfo");
        });
      }
    });
  };

  //validations
  let onlyLetters = (rule, value, callback) => {
    try {
      const regex = /[а-яА-Я]+/g;
      if (!value.match(regex)) {
        throw new Error("Вводить можно только буквы");
      }
      callback();
    } catch (err) {
      callback(err);
    }
  };

  let onlyNum = (rule, value, callback) => {
    try {
      const regex = /^[0-9]+$/;
      if (!value.match(regex)) {
        throw new Error("Вводить можно только цифры");
      }
      callback();
    } catch (err) {
      callback(err);
    }
  };

  let disabledDate = current => {
    return current && current > moment().endOf("day");
  };

  return (
    <>
      <Link to="/cabinet/sfo">
        <Icon type="left" />
      </Link>
      <span className="add">Создание специальной финансовой организации</span>

      <Form layout="horizontal" onSubmit={handleSubmit} className="sfoAdd">
        <span className="mainTextForm">Сведения об организации</span>
        <Form.Item className="firstSection">
          <Form.Item label="Наименование" {...formItemLayout}>
            {getFieldDecorator("name", {
              rules: [
                {
                  required: true,
                  message: "Введите наименование"
                },
                {
                  validator: onlyLetters
                }
              ]
            })(
              <Input
                type="text"
                name="name"
                onChange={nameHandleChange}
                className="usersFormInput"
              />
            )}
          </Form.Item>

          <Form.Item label="Кратко" {...formItemLayout}>
            {getFieldDecorator("short", {
              rules: [
                {
                  required: true,
                  message: "Введите наименование кратко"
                },
                {
                  validator: onlyLetters
                }
              ]
            })(
              <Input
                type="text"
                name="short"
                onChange={shortHandleChange}
                className="usersFormInput"
              />
            )}
          </Form.Item>

          <Form.Item label="Резидент" {...formItemLayout}>
            <Checkbox
              name="isResident"
              onChange={isResidentHandleChange}
            ></Checkbox>
          </Form.Item>

          <Form.Item label="Уставной фонд" {...formItemLayout}>
            <Input
              type="number"
              name="fund"
              onChange={fundHandleChange}
              className="usersFormInput"
            />
          </Form.Item>

          <Form.Item label="УНП" {...formItemLayout}>
            {getFieldDecorator("unp", {
              rules: [
                {
                  required: true,
                  message: "Введите УНП"
                },
                {
                  validator: onlyNum
                }
              ]
            })(
              <Input
                type="text"
                name="unp"
                onChange={unpHandleChange}
                className="usersFormInput"
              />
            )}
          </Form.Item>

          <Form.Item label="Банк" {...formItemLayout}>
            <Select name="bank" onChange={bankHandleChange}>
              <Option value="Альфа Банк">Альфа Банк</Option>
              <Option value="БСБ">БСБ</Option>
              <Option value="Приор">Приор</Option>
            </Select>
          </Form.Item>
        </Form.Item>

        <span className="mainTextForm">Сведения о регистрации</span>

        <Form.Item className="secondSection">
          <Form.Item label="Дата гос. регистрации" {...formItemLayout}>
            {getFieldDecorator("date", {
              rules: [
                {
                  required: true,
                  message: "Введите дату регистрации"
                }
              ]
            })(
              <DatePicker
                onChange={dateHandleChange}
                format={"DD.MM.YYYY"}
                disabledDate={disabledDate}
              />
            )}
          </Form.Item>

          <Form.Item label="Кем выдан" {...formItemLayout}>
            {getFieldDecorator("issuedBy", {
              rules: [
                {
                  required: true,
                  message: "Введите, кем выдан"
                },
                {
                  validator: onlyLetters
                }
              ]
            })(
              <Input
                type="text"
                name="issuedBy"
                onChange={issuedByHandleChange}
                className="usersFormInput"
              />
            )}
          </Form.Item>

          <Form.Item label="Налоговая инспекция" {...formItemLayout}>
            <Select name="tax" onChange={taxHandleChange}>
              <Option value="ОАО">ОАО</Option>
            </Select>
          </Form.Item>
        </Form.Item>

        <Form.Item>
          <Link to="/cabinet/sfo">
            <Button type="primary" className="cancelButton">
              <span className="addBtnText">Отмена</span>
            </Button>
          </Link>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="btnUsersForm">
            <span className="addBtnText">Добавить</span>
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

const WrappedSfoAdd = Form.create({ name: "sfoAdd" })(SfoAdd);

export default WrappedSfoAdd;
