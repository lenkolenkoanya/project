import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import axios from "axios";
import { Form, Input, Button, Checkbox, Select, DatePicker, Icon } from "antd";
import { useParams, useHistory, Link } from "react-router-dom";
import moment from "moment";

const { Option } = Select;

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function SfoEdit(props) {
  let history = useHistory();
  const { getFieldDecorator, setFieldsValue } = props.form;
  let { sfoId } = useParams();

  const [dataLoaded, setDataLoaded] = useState(false);

  const [name, setName] = useState("");
  const [short, setShort] = useState("");
  const [isResident, setIsResident] = useState(false);
  const [fund, setFund] = useState(0);
  const [unp, setUnp] = useState(0);
  const [bank, setBank] = useState("");
  const [date, setDate] = useState("");
  const [issuedBy, setIssuedBy] = useState("");
  const [tax, setTax] = useState("");

  let nameHandleChange = e => {
    setName(e.target.value);
  };

  let shortHandleChange = e => {
    setShort(e.target.value);
  };

  let fundHandleChange = e => {
    setFund(e.target.value);
  };

  let unpHandleChange = e => {
    setUnp(e.target.value);
  };

  let dateHandleChange = date => {
    date = date.format("DD.MM.YYYY");
    setDate(date);
  };

  let issuedByHandleChange = e => {
    setIssuedBy(e.target.value);
  };

  let isResidentHandleChange = e => {
    setIsResident(e.target.checked);
  };

  let taxHandleChange = value => {
    setTax(value);
  };

  let bankHandleChange = value => {
    setBank(value);
  };

  useEffect(() => {
    axios.get("http://localhost:3000/sfo/" + sfoId).then(resp => {
      let newData = resp.data;
      setName(newData.name);
      setShort(newData.short);
      setIsResident(newData.isResident);
      setFund(newData.fund);
      setUnp(newData.unp);
      setBank(newData.bank);
      setDate(newData.date);
      setIssuedBy(newData.issuedBy);
      setTax(newData.tax);

      setDataLoaded(true);

      setFieldsValue({
        name: newData.name,
        short: newData.short,
        unp: newData.unp,
        date: moment(newData.date, "DD.MM.YYYY"),
        issuedBy: newData.issuedBy
      });
    });
  }, [sfoId, setFieldsValue]);

  let handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        const sfo = {
          id: sfoId,
          name: name,
          short: short,
          isResident: isResident,
          fund: fund,
          unp: unp,
          bank: bank,
          date: date,
          issuedBy: issuedBy,
          tax: tax
        };

        axios.put("http://localhost:3000/sfo/" + sfoId, sfo).then(() => {
          history.push("/cabinet/sfo");
        });
      }
    });
  };

  //validations
  let onlyLetters = (rule, value, callback) => {
    try {
      const regex = /[а-яА-Я]+/g;
      if (!value.match(regex)) {
        throw new Error("Вводить можно только буквы");
      }
      callback();
    } catch (err) {
      callback(err);
    }
  };

  let onlyNum = (rule, value, callback) => {
    try {
      const regex = /^[0-9]+$/;
      if (!value.match(regex)) {
        throw new Error("Вводить можно только цифры");
      }
      callback();
    } catch (err) {
      callback(err);
    }
  };

  let disabledDate = current => {
    return current && current > moment().endOf("day");
  };

  return (
    <>
      {dataLoaded === true ? (
        <>
          <Link to="/cabinet/sfo">
            <Icon type="left" />
          </Link>
          <span className="add">
            Создание специальной финансовой организации
          </span>

          <Form layout="horizontal" onSubmit={handleSubmit} className="sfoEdit">
            <span className="mainTextForm">Сведения об организации</span>
            <Form.Item className="firstSectionEdit">
              <Form.Item>
                <Input
                  type="text"
                  name="id"
                  value={sfoId}
                  style={{ display: "none" }}
                />
              </Form.Item>
              <Form.Item label="Наименование" {...formItemLayout}>
                {getFieldDecorator("name", {
                  rules: [
                    {
                      required: true,
                      message: "Введите наименование"
                    },
                    {
                      validator: onlyLetters
                    }
                  ]
                })(
                  <Input
                    type="text"
                    name="name"
                    onChange={nameHandleChange}
                    className="usersFormInput"
                    // value={name}
                  />
                )}
              </Form.Item>

              <Form.Item label="Кратко" {...formItemLayout}>
                {getFieldDecorator("short", {
                  rules: [
                    {
                      required: true,
                      message: "Введите наименование кратко"
                    },
                    {
                      validator: onlyLetters
                    }
                  ]
                })(
                  <Input
                    type="text"
                    name="short"
                    onChange={shortHandleChange}
                    className="usersFormInput"
                    // value={short}
                  />
                )}
              </Form.Item>

              <Form.Item label="Резидент" {...formItemLayout}>
                <Checkbox
                  name="isResident"
                  onChange={isResidentHandleChange}
                  checked={isResident}
                ></Checkbox>
              </Form.Item>

              <Form.Item label="Уставной фонд" {...formItemLayout}>
                <Input
                  type="number"
                  name="fund"
                  onChange={fundHandleChange}
                  className="usersFormInput"
                  value={fund}
                />
              </Form.Item>

              <Form.Item label="УНП" {...formItemLayout}>
                {getFieldDecorator("unp", {
                  rules: [
                    {
                      required: true,
                      message: "Введите УНП"
                    },
                    {
                      validator: onlyNum
                    }
                  ]
                })(
                  <Input
                    type="text"
                    name="unp"
                    onChange={unpHandleChange}
                    className="usersFormInput"
                    // value={unp}
                  />
                )}
              </Form.Item>

              <Form.Item label="Банк" {...formItemLayout}>
                <Select name="bank" onChange={bankHandleChange} value={bank}>
                  <Option value="Альфа Банк">Альфа Банк</Option>
                  <Option value="БСБ">БСБ</Option>
                  <Option value="Приор">Приор</Option>
                </Select>
              </Form.Item>
            </Form.Item>

            <span className="mainTextForm">Сведения о регистрации</span>

            <Form.Item className="secondSection">
              <Form.Item label="Дата гос. регистрации" {...formItemLayout}>
                {getFieldDecorator("date", {
                  rules: [
                    {
                      required: true,
                      message: "Введите дату"
                    }
                  ]
                })(
                  <DatePicker
                    onChange={dateHandleChange}
                    format={"DD.MM.YYYY"}
                    // value={date === "" ? null : moment(date, "DD.MM.YYYY")}
                    disabledDate={disabledDate}
                  />
                )}
              </Form.Item>

              <Form.Item label="Кем выдан" {...formItemLayout}>
                {getFieldDecorator("issuedBy", {
                  rules: [
                    {
                      required: true,
                      message: "Введите, кем выдан"
                    },
                    {
                      validator: onlyLetters
                    }
                  ]
                })(
                  <Input
                    type="text"
                    name="issuedBy"
                    onChange={issuedByHandleChange}
                    className="usersFormInput"
                    // value={issuedBy}
                  />
                )}
              </Form.Item>

              <Form.Item label="Налоговая инспекция" {...formItemLayout}>
                <Select name="tax" onChange={taxHandleChange} value={tax}>
                  <Option value="ОАО">ОАО</Option>
                </Select>
              </Form.Item>
            </Form.Item>

            <Form.Item>
              <Link to="/cabinet/sfo">
                <Button type="primary" className="cancelButton">
                  <span className="addBtnText">Отмена</span>
                </Button>
              </Link>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="btnUsersForm">
                <span className="addBtnText">Сохранить</span>
              </Button>
            </Form.Item>
          </Form>
        </>
      ) : null}
    </>
  );
}

const WrappedSfoEdit = Form.create({ name: "sfoEdit" })(SfoEdit);

export default WrappedSfoEdit;
