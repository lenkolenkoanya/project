import React from "react";
import { Typography } from "antd";
import "antd/dist/antd.css";

function User() {
  const { Text } = Typography;

  let userFirstName = window.localStorage.getItem("firstName");
  let userSecondName = window.localStorage.getItem("lastName");

  return (
    <Text>
      {userFirstName} {userSecondName}
    </Text>
  );
}

export default User;
