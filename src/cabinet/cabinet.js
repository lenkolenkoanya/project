import React from "react";
import { Switch, Route } from "react-router-dom";
import { Layout, Button, Icon } from "antd";
import TableUsers from "../users/users";
import WrappedNewForm from "../users/userAdd";
import Documents from "../documents/doc";
import WrappedAddDoc from "../documents/add";
import Edit from "../documents/edit";
import NewSider from "../users/sider";
import { useHistory } from "react-router-dom";
import User from "./user";
import UsersEdit from "../users/usersEdit";
import Settings from "../settings/settings";
import WrappedPasswordChange from "../users/passwordChange";
import Sfo from "../sfo/sfo.js";
import WrappedSfoAdd from "../sfo/sfoAdd.js";
import WrappedSfoEdit from "../sfo/sfoEdit.js";

const { Content, Header } = Layout;

function Cabinet() {
  let history = useHistory();

  let handleClick = e => {
    e.preventDefault();
    localStorage.clear();
    history.push("/");
  };

  return (
    <Layout>
      <NewSider />
      <Layout>
        <Header>
          <Icon type="user" />
          <User />
          <Button type="link" ghost onClick={handleClick}>
            <Icon type="logout" />
          </Button>
        </Header>
        <Content>
          <Switch>
            <Route exact path="/cabinet/users" children={<TableUsers />} />
            <Route exact path="/cabinet/documents" children={<Documents />} />
            <Route exact path="/cabinet/settings" children={<Settings />} />
            <Route exact path="/cabinet/sfo" children={<Sfo />} />
            <Route
              exact
              path="/cabinet/users/form"
              children={<WrappedNewForm />}
            />
            <Route
              exact
              path="/cabinet/users/edit/:userId"
              children={<UsersEdit />}
            />
            <Route
              exact
              path="/cabinet/users/password_change/:userId"
              children={<WrappedPasswordChange />}
            />
            <Route
              exact
              path="/cabinet/documents/add"
              children={<WrappedAddDoc />}
            />
            <Route
              exact
              path="/cabinet/documents/edit/:docId"
              children={<Edit />}
            />
            <Route exact path="/cabinet/sfo/add" children={<WrappedSfoAdd />} />
            <Route
              exact
              path="/cabinet/sfo/edit/:sfoId"
              children={<WrappedSfoEdit />}
            />
          </Switch>
        </Content>
      </Layout>
    </Layout>
  );
}

export default Cabinet;
