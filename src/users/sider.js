import React from "react";
import "antd/dist/antd.css";
import { Layout, Menu, Icon } from "antd";
import { Link } from "react-router-dom";
import MinLogo from "../images/logo.jpg";

const { Sider } = Layout;

class NewSider extends React.Component {
  render() {
    return (
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="mainLogo" />
        <Menu theme="light" mode="inline">
          <img src={MinLogo} alt="minlogo" className="minLogo" />

          <Menu.Item key="2">
            <Link to="/cabinet">
              <Icon type="home" />
              <span className="nav-text">Рабочий стол</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="3">
            <Link to="/cabinet/users">
              <Icon type="user" />
              <span className="nav-text">Пользователи</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="4">
            <Link to="/cabinet/sfo">
              <Icon type="bank" />
              <span className="nav-text">СФО</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="5">
            <Link to="/cabinet/documents">
              <Icon type="table" />
              <span className="nav-text">Документы</span>
            </Link>
          </Menu.Item>

          <Menu.Item key="6">
            <Link to="/cabinet/settings">
              <Icon type="setting" />
              <span className="nav-text">Настройки</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
    );
  }
}

export default NewSider;
