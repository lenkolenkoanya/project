import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";
import axios from "axios";
import { useHistory, useParams, Link } from "react-router-dom";

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function PasswordChange(props) {
  const { getFieldDecorator } = props.form;
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [secondNewPassword, setSecondNewPassword] = useState("");

  let { userId } = useParams();
  let history = useHistory();

  let passwordHandleChange = e => {
    setPassword(e.target.value);
  };

  let newPasswordHandleChange = e => {
    setNewPassword(e.target.value);
  };

  let secondNewPasswordHandleChange = e => {
    setSecondNewPassword(e.target.value);
  };

  useEffect(() => {
    axios.get("http://localhost:3000/users/" + userId).then(resp => {
      let newData = resp.data;
      setPassword(newData.password);
    });
  }, [userId]);

  let handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        axios.get("http://localhost:3000/users").then(resp => {
          let newData = resp.data;
          let userFirstName = window.localStorage.getItem("firstName");

          let passwordIsCorrect = false;
          for (let i = 0; i < newData.length; i++) {
            if (newData[i].firstName === userFirstName) {
              if (
                newData[i].password ===
                props.form.getFieldValue("currentPassword")
              ) {
                passwordIsCorrect = true;
                break;
              }
            }
          }

          if (passwordIsCorrect) {
            const newPasswordData = {
              id: userId,
              password: secondNewPassword
            };

            axios
              .patch("http://localhost:3000/users/" + userId, newPasswordData)
              .then(() => {
                history.push("/cabinet/users");
              });
          } else {
            props.form.setFields({
              currentPassword: {
                errors: [new Error("Неверный текущий пароль")]
              }
            });
          }
        });
      }
    });
  };

  //second validate
  let compareToFirstPassword = (rule, value, callback) => {
    try {
      if (value !== props.form.getFieldValue("newPassword")) {
        throw new Error("Пароли не совпадают");
      }
      callback();
    } catch (err) {
      callback(err);
    }
  };

  return (
    <>
      <span className="add">Изменение пароля</span>
      <Form
        layout="horizontal"
        onSubmit={handleSubmit}
        className="newPasswordForm"
      >
        <Form.Item>
          <Input value={userId} style={{ display: "none" }} />
        </Form.Item>

        <Form.Item label="Текущий пароль" {...formItemLayout}>
          {getFieldDecorator("currentPassword", {
            rules: [
              {
                required: true,
                message: "Введите текущий пароль"
              }
            ]
          })(
            <Input
              type="password"
              name="currentPassword"
              onChange={passwordHandleChange}
              className="usersFormInput"
              maxLength={4}
              pattern="[0-9]*"
            />
          )}
        </Form.Item>

        <Form.Item label="Новый пароль" {...formItemLayout}>
          {getFieldDecorator("newPassword", {
            rules: [
              {
                required: true,
                message: "Введите новый пароль"
              }
            ]
          })(
            <Input
              type="password"
              name="newPassword"
              onChange={newPasswordHandleChange}
              className="usersFormInput"
              maxLength={4}
              pattern="[0-9]*"
            />
          )}
        </Form.Item>

        <Form.Item label="Повторите новый пароль" {...formItemLayout}>
          {getFieldDecorator("secondNewPassword", {
            rules: [
              {
                required: true,
                message: "Повторите новый пароль"
              },
              {
                validator: compareToFirstPassword
              }
            ]
          })(
            <Input
              type="password"
              name="secondNewPassword"
              onChange={secondNewPasswordHandleChange}
              className="usersFormInput"
              maxLength={4}
              pattern="[0-9]*"
            />
          )}
        </Form.Item>

        <Form.Item>
          <Link to="/cabinet/users">
            <Button type="primary" className="cancelButton">
              <span className="addBtnText">Отмена</span>
            </Button>
          </Link>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="btnUsersForm">
            <span className="addBtnText">Изменить</span>
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

const WrappedPasswordChange = Form.create({ name: "passwordChange" })(
  PasswordChange
);

export default WrappedPasswordChange;
