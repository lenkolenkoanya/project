import React, { useState, useEffect } from "react";
import axios from "axios";
import "antd/dist/antd.css";
import { Table, Button, Icon } from "antd";
import { Link } from "react-router-dom";

function TableUsers() {
  const [users, setUsers] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:3000/users").then(resp => {
      const users = resp.data;
      setUsers(users);
    });
  }, []);

  //onclick edit
  let handleClick = () => {
    setSelectedRowKeys(selectedRowKeys);
  };

  //select checkbox
  let onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id"
    },
    {
      title: "Имя",
      dataIndex: "firstName",
      key: "firstName"
    },
    {
      title: "Фамилия",
      dataIndex: "lastName",
      key: "lastName"
    },
    {
      title: "Почта",
      dataIndex: "email",
      key: "email"
    },
    {
      title: "Логин",
      dataIndex: "login",
      key: "login"
    },
    {
      title: "Пароль",
      dataIndex: "password",
      key: "password"
    },
    {
      title: "Дата рождения",
      dataIndex: "birth",
      key: "birth"
    }
  ];

  const data = users.map(user => {
    return {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      login: user.login,
      password: user.password,
      birth: user.birth
    };
  });

  const hasSelected = selectedRowKeys.length > 0;

  return (
    <>
      <span className="addSfoTable">Пользователи</span>
      <Link to="/cabinet/users">
        <Button type="primary" className="refreshButton">
          <span className="addBtnText">Обновить</span>
        </Button>
      </Link>
      <div className="sfoTable">
        <Link to="/cabinet/users/form">
          <Button type="link" className="buttonAdd">
            <Icon type="plus-circle" />
            <span className="textBtn">Создать</span>
          </Button>
        </Link>
        <Link to={`/cabinet/users/edit/${selectedRowKeys[0]}`}>
          <Button
            type="link"
            onClick={handleClick}
            disabled={!hasSelected}
            className="buttonAdd"
          >
            <Icon type="edit" />
            <span className="textBtn">Изменить</span>
          </Button>
        </Link>
        <Link to={`/cabinet/users/password_change/${selectedRowKeys[0]}`}>
          <Button
            type="link"
            onClick={handleClick}
            disabled={!hasSelected}
            className="buttonAdd"
          >
            <Icon type="key" />
            <span className="textBtn">Изменить пароль</span>
          </Button>
        </Link>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          rowKey="id"
          className="table"
        />
      </div>
    </>
  );
}

export default TableUsers;
