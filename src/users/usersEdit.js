import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, DatePicker } from "antd";
import axios from "axios";
import { useHistory, useParams, Link } from "react-router-dom";
import moment from "moment";

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function UsersEdit() {
  let { userId } = useParams();
  let history = useHistory();

  const [dataLoaded, setDataLoaded] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [birth, setBirth] = useState("");

  let firstNameHandleChange = e => {
    setFirstName(e.target.value);
  };

  let lastNameHandleChange = e => {
    setLastName(e.target.value);
  };

  let emailHandleChange = e => {
    setEmail(e.target.value);
  };

  let loginHandleChange = e => {
    setLogin(e.target.value);
  };

  let passwordHandleChange = e => {
    setPassword(e.target.value);
  };

  let birthHandleChange = date => {
    date = date.format("YYYY-MM-DD");
    setBirth(date);
  };

  useEffect(() => {
    axios.get("http://localhost:3000/users/" + userId).then(resp => {
      var user = resp.data;
      setFirstName(user.firstName);
      setLastName(user.lastName);
      setEmail(user.email);
      setLogin(user.login);
      setPassword(user.password);
      setBirth(user.birth);

      setDataLoaded(true);
    });
  }, [userId]);

  let handleSubmit = e => {
    e.preventDefault();
    axios.put("http://localhost:3000/users/" + userId, {
      id: userId,
      firstName: firstName,
      lastName: lastName,
      email: email,
      login: login,
      password: password,
      birth: birth
    });

    history.push("/cabinet/users");
  };

  return (
    <>
      {dataLoaded === true ? (
        <>
          <span className="add">Редактирование пользователя</span>
          <Form
            layout="horizontal"
            onSubmit={handleSubmit}
            className="userEditForm"
          >
            <Form.Item>
              <Input
                type="text"
                name="id"
                value={userId}
                style={{ display: "none" }}
              />
            </Form.Item>
            <Form.Item label="Имя" {...formItemLayout}>
              <Input
                type="text"
                name="firstName"
                onChange={firstNameHandleChange}
                value={firstName}
                className="usersFormInput"
              />
            </Form.Item>

            <Form.Item label="Фамилия" {...formItemLayout}>
              <Input
                type="text"
                name="lastName"
                onChange={lastNameHandleChange}
                value={lastName}
                className="usersFormInput"
              />
            </Form.Item>

            <Form.Item label="Почта" {...formItemLayout}>
              <Input
                type="text"
                name="email"
                onChange={emailHandleChange}
                value={email}
                className="usersFormInput"
              />
            </Form.Item>

            <Form.Item label="Логин" {...formItemLayout}>
              <Input
                type="text"
                name="login"
                onChange={loginHandleChange}
                value={login}
                className="usersFormInput"
              />
            </Form.Item>

            <Form.Item label="Пароль" {...formItemLayout}>
              <Input
                type="text"
                name="password"
                onChange={passwordHandleChange}
                value={password}
                className="usersFormInput"
              />
            </Form.Item>

            <Form.Item label="Дата рождения" {...formItemLayout}>
              <DatePicker
                onChange={birthHandleChange}
                format={"YYYY-MM-DD"}
                value={moment(birth, "YYYY-MM-DD")}
                // value={birth === "" ? null : moment(birth, "YYYY-MM-DD")}
                className="usersFormInput"
              />
            </Form.Item>

            <Form.Item>
              <Link to="/cabinet/users">
                <Button type="primary" className="cancelButton">
                  <span className="addBtnText">Отмена</span>
                </Button>
              </Link>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="btnUsersForm">
                <span className="addBtnText">Сохранить</span>
              </Button>
            </Form.Item>
          </Form>
        </>
      ) : null}
    </>
  );
}

export default UsersEdit;
