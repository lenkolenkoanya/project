import React, { useState } from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, DatePicker, Icon } from "antd";
import axios from "axios";
import { useHistory, Link } from "react-router-dom";

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function NewForm(props) {
  let history = useHistory();
  const { getFieldDecorator } = props.form;
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [birth, setBirth] = useState("");

  let firstNameHandleChange = e => {
    setFirstName(e.target.value);
  };

  let lastNameHandleChange = e => {
    setLastName(e.target.value);
  };

  let emailHandleChange = e => {
    setEmail(e.target.value);
  };

  let loginHandleChange = e => {
    setLogin(e.target.value);
  };

  let passwordHandleChange = e => {
    setPassword(e.target.value);
  };

  let birthHandleChange = date => {
    date = date.format("YYYY-MM-DD");
    setBirth(date);
  };

  let handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        const user = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          login: login,
          password: password,
          birth: birth
        };

        axios.post("http://localhost:3000/users", user);

        history.push("/cabinet/users");
      }
    });
  };

  return (
    <>
      <Link to="/cabinet/sfo">
        <Icon type="left" />
      </Link>
      <span className="add"> Добавить пользователя </span>
      <Form
        layout="horizontal"
        onSubmit={handleSubmit}
        className="usersAddForm"
      >
        <Form.Item label="Имя" {...formItemLayout}>
          {getFieldDecorator("firstName", {
            rules: [
              {
                required: true,
                message: "Введите имя"
              }
            ]
          })(<Input onChange={firstNameHandleChange} />)}
        </Form.Item>

        <Form.Item label="Фамилия" {...formItemLayout}>
          {getFieldDecorator("lastName", {
            rules: [
              {
                required: true,
                message: "Введите фамилию"
              }
            ]
          })(<Input onChange={lastNameHandleChange} />)}
        </Form.Item>

        <Form.Item label="Почта" {...formItemLayout}>
          {getFieldDecorator("email", {
            rules: [
              {
                required: true,
                message: "Введите почту"
              }
            ]
          })(<Input onChange={emailHandleChange} />)}
        </Form.Item>

        <Form.Item label="Логин" {...formItemLayout}>
          {getFieldDecorator("login", {
            rules: [
              {
                required: true,
                message: "Введите логин"
              }
            ]
          })(<Input onChange={loginHandleChange} />)}
        </Form.Item>

        <Form.Item label="Пароль" {...formItemLayout}>
          {getFieldDecorator("password", {
            rules: [
              {
                required: true,
                message: "Введите пароль"
              }
            ]
          })(<Input onChange={passwordHandleChange} pattern="[0-9]*" />)}
        </Form.Item>

        <Form.Item label="Дата рождения" {...formItemLayout}>
          {getFieldDecorator("birth", {
            rules: [
              {
                required: true,
                message: "Введите дату рождения"
              }
            ]
          })(<DatePicker onChange={birthHandleChange} format={"YYYY-MM-DD"} />)}
        </Form.Item>

        <Form.Item>
          <Link to="/cabinet/users">
            <Button type="primary" className="cancelButton">
              <span className="addBtnText">Отмена</span>
            </Button>
          </Link>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="btnUsersForm">
            <span className="addBtnText">Добавить</span>
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

const WrappedNewForm = Form.create({ name: "newForm" })(NewForm);

export default WrappedNewForm;
