import React, { useState } from "react";
import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";
import axios from "axios";
import { useHistory, Link } from "react-router-dom";

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function AddDoc(props) {
  let history = useHistory();
  const [name, setName] = useState("");
  const [amount, setAmount] = useState(0);
  const { getFieldDecorator } = props.form;

  let nameHandleChange = e => {
    setName(e.target.value);
  };

  let amountHandleChange = e => {
    setAmount(e.target.value);
  };

  let handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        const document = {
          name: name,
          amount: amount
        };

        axios.post("http://localhost:3000/documents", document);

        history.push("/cabinet/documents");
      }
    });
  };

  return (
    <>
      <span className="add">Добавить документ</span>
      <Form layout="horizontal" onSubmit={handleSubmit} className="docAddForm">
        <Form.Item label="Имя" {...formItemLayout}>
          {getFieldDecorator("name", {
            rules: [
              {
                required: true,
                message: "Введите имя"
              }
            ]
          })(
            <Input
              type="text"
              name="name"
              onChange={nameHandleChange}
              className="usersFormInput"
            />
          )}
        </Form.Item>

        <Form.Item label="Количество" {...formItemLayout}>
          {getFieldDecorator("amount", {
            rules: [
              {
                required: true,
                message: "Введите количество"
              }
            ]
          })(
            <Input
              type="text"
              pattern="[0-9]*"
              name="amount"
              onChange={amountHandleChange}
              className="usersFormInput"
            />
          )}
        </Form.Item>

        <Form.Item>
          <Link to="/cabinet/documents">
            <Button type="primary" className="cancelButton">
              <span className="addBtnText">Отмена</span>
            </Button>
          </Link>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="btnUsersForm">
            <span className="addBtnText">Добавить</span>
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

const WrappedAddDoc = Form.create({ name: "addDoc" })(AddDoc);

export default WrappedAddDoc;
