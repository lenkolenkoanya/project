import React, { useState, useEffect } from "react";
import axios from "axios";
import "antd/dist/antd.css";
import { Table, Button, Icon } from "antd";
import { Link } from "react-router-dom";

function Documents() {
  const [documents, setDocuments] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:3000/documents").then(resp => {
      const documents = resp.data;
      setDocuments(documents);
    });
  }, []);

  let handleClick = () => {
    setSelectedRowKeys(selectedRowKeys);
  };

  //select checkbox
  let onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id"
    },
    {
      title: "Имя",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Количество",
      dataIndex: "amount",
      key: "amount"
    }
  ];

  const data = documents.map(doc => {
    return {
      id: doc.id,
      name: doc.name,
      amount: doc.amount
    };
  });

  const hasSelected = selectedRowKeys.length > 0;

  return (
    <>
      <span className="addSfoTable">Документы</span>
      <Link to="/cabinet/documents">
        <Button type="primary" className="refreshButton">
          <span className="addBtnText">Обновить</span>
        </Button>
      </Link>
      <div className="sfoTable">
        <Link to="/cabinet/documents/add">
          <Button type="link" className="buttonAdd">
            <Icon type="plus-circle" />
            <span className="textBtn">Создать</span>
          </Button>
        </Link>
        <Link to={`/cabinet/documents/edit/${selectedRowKeys[0]}`}>
          <Button
            type="link"
            onClick={handleClick}
            disabled={!hasSelected}
            className="buttonAdd"
          >
            <Icon type="edit" />
            <span className="textBtn">Изменить</span>
          </Button>
        </Link>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          rowKey="id"
          className="table"
        />
      </div>
    </>
  );
}

export default Documents;
