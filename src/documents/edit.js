import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";
import axios from "axios";
import { useParams, useHistory, Link } from "react-router-dom";

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
};

function Edit() {
  let { docId } = useParams();
  let history = useHistory();

  const [name, setName] = useState("");
  const [amount, setAmount] = useState(0);

  let nameHandleChange = e => {
    setName(e.target.value);
  };

  let amountHandleChange = e => {
    setAmount(e.target.value);
  };

  useEffect(() => {
    axios.get("http://localhost:3000/documents/" + docId).then(resp => {
      let document = resp.data;
      setAmount(document.amount);
      setName(document.name);
    });
  }, [docId]);

  let handleSubmit = e => {
    e.preventDefault();
    axios.put("http://localhost:3000/documents/" + docId, {
      id: docId,
      name: name,
      amount: amount
    });

    history.push("/cabinet/documents");
  };

  return (
    <>
      <span className="add">Редактировать документ</span>
      <Form layout="horizontal" onSubmit={handleSubmit} className="docEditForm">
        <Form.Item>
          <Input
            type="text"
            name="name"
            value={docId}
            className="usersFormInput"
            style={{ display: "none" }}
          />
        </Form.Item>

        <Form.Item label="Имя" {...formItemLayout}>
          <Input
            type="text"
            name="name"
            onChange={nameHandleChange}
            value={name}
            className="usersFormInput"
          />
        </Form.Item>

        <Form.Item label="Количество" {...formItemLayout}>
          <Input
            type="text"
            pattern="[0-9]*"
            name="amount"
            onChange={amountHandleChange}
            value={amount}
            className="usersFormInput"
          />
        </Form.Item>

        <Form.Item>
          <Link to="/cabinet/documents">
            <Button type="primary" className="cancelButton">
              <span className="addBtnText">Отмена</span>
            </Button>
          </Link>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="btnUsersForm">
            <span className="addBtnText">Сохранить</span>
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

export default Edit;
