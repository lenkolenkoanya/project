import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";
import Logo from "../images/bpb.jpg";
import { Layout } from "antd";
import axios from "axios";
import { useHistory } from "react-router-dom";

function HomeForm(props) {
  const { Content } = Layout;
  const history = useHistory();
  const { getFieldDecorator } = props.form;

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        axios.get("http://localhost:3000/users").then(resp => {
          const users = resp.data;

          const dataFromJson = users.map(i => {
            return {
              firstName: i.firstName,
              lastName: i.lastName,
              login: i.login,
              password: i.password
            };
          });

          for (let i = 0; i < dataFromJson.length; i++) {
            if (
              values.login === dataFromJson[i].login.toString() &&
              values.password === dataFromJson[i].password.toString()
            ) {
              window.localStorage.setItem(
                "firstName",
                dataFromJson[i].firstName
              );
              window.localStorage.setItem("lastName", dataFromJson[i].lastName);

              history.push("/cabinet");
              break;
            } else {
              props.form.setFields({
                login: {
                  errors: [new Error("Логин не подходит")]
                },
                password: {
                  errors: [new Error("Пароль не подходит")]
                }
              });
            }
          }
          window.localStorage.setItem("isLoggedIn", true);
        });
      }
    });
  };

  return (
    <Content>
      <img src={Logo} alt="logo" className="logo" />
      <p className="modul">Модуль «СПЕЦДЕПОЗИТАРИЙ»</p>

      <Form layout="vertical" onSubmit={handleSubmit} className="loginForm">
        <Form.Item label="Логин">
          {getFieldDecorator("login", {
            rules: [
              {
                required: true,
                message: "Введите логин"
              }
            ]
          })(<Input />)}
        </Form.Item>

        <Form.Item label="Пароль">
          {getFieldDecorator("password", {
            rules: [
              {
                required: true,
                message: "Введите пароль"
              }
            ]
          })(<Input.Password maxLength={4} />)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="button">
            <span className="butText">Войти</span>
          </Button>
        </Form.Item>
      </Form>
    </Content>
  );
}

const WrappedHomeForm = Form.create({ name: "homeForm" })(HomeForm);

export default WrappedHomeForm;
