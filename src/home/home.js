import React from "react";
import "antd/dist/antd.css";
import { Layout } from "antd";
import "../images/background.jpg";
import ST from "../images/st.jpg";
import WrappedHomeForm from "./homeForm";
import { useHistory } from "react-router-dom";

const { Footer } = Layout;

function Home() {
  let history = useHistory();
  let userLog = window.localStorage.getItem("isLoggedIn");
  if (userLog) {
    history.push("/cabinet");
  }
  return (
    <Layout className="myLayout">
      <WrappedHomeForm />
      <Footer>
        <span className="footerText">©2020 ОАО «Белагропромбанк»</span>
        <span className="secondFooterText">Разработано</span>
        <img src={ST} alt="st" className="st" />
      </Footer>
    </Layout>
  );
}

export default Home;
