import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";
import Home from "./home";
import Cabinet from "../cabinet/cabinet";

function App() {
  return (
    <BrowserRouter forceRefresh={true}>
      <Switch>
        <Route exact path="/" children={<Home />} />
        <Route path="/cabinet" children={<Cabinet />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
